
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		// Change player's weapon cooldown to allow ship to shoot faster
		m_cooldownSeconds = 0.1;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		// Checks if the ship is active and also if the firing cooldown has finished
		{
			if (triggerType.Contains(GetTriggerType()))
			// Checks which kind of trigger the ship is performing; such as primary, secondary, special, etc
			{
				Projectile *pProjectile = GetProjectile();
				// Gets the projectile that matches to that kind of trigger
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);
					// Fires off the projectile
					m_cooldown = m_cooldownSeconds;
					// Sets the firing cooldown after firing
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};